Advent of Code 2022
===================

Solutions for [Advent of Code 2022](https://adventofcode.com/2022) written in [Erlang](https://www.erlang.org/).

Build
-----

    $ rebar3 compile

Run
---

Solve all days:

    $ rebar3 shell
    1> aoc_app:solve_all().

Solve single day (e.g. day 1):

    $ rebar3 shell
    1> aoc_app:solve(day_01).