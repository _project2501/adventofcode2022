-module(day_11).
-export([solve/2]).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

-record(monkey, {
    items :: [non_neg_integer()],
    op_fun :: fun((non_neg_integer()) -> non_neg_integer()),
    test_div :: non_neg_integer(),
    test_true :: non_neg_integer(),
    test_false :: non_neg_integer()
}).
-type monkey() :: #monkey{}.
-type worry_fun() :: fun((non_neg_integer()) -> non_neg_integer()).
-type(inspect_counter()) :: maps:map().

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec update_monkey(string(), monkey()) -> monkey().
update_monkey(Line, Monkey) ->
    Tokens = string:tokens(Line, " "),
    case hd(Tokens) of
        "Monkey" -> Monkey;
        "Starting" ->
            ValuesStr = lists:sublist(Tokens, 3, length(Tokens)),
            Values = lists:map(
                fun(El) ->
                    list_to_integer(string:strip(El, both, $,))
                end, ValuesStr),
            Monkey#monkey{items = Values};
        "Operation:" ->
            Len = length(Tokens),
            OpFun = case lists:nth(Len - 1, Tokens) of
                "+" ->
                    case lists:last(Tokens) of
                        "old" ->
                            fun(X) -> X + X end;
                        ValStr ->
                            Val = list_to_integer(ValStr),
                            fun(X) -> X + Val end
                    end;
                "*" ->
                    case lists:last(Tokens) of
                        "old" ->
                            fun(X) -> X * X end;
                        ValStr ->
                            Val = list_to_integer(ValStr),
                            fun(X) -> X * Val end
                    end
            end,
            Monkey#monkey{
                op_fun = OpFun
            };
        "Test:" ->
            Value = list_to_integer(lists:last(Tokens)),
            Monkey#monkey{test_div = Value};
        "If" ->
            Value = list_to_integer(lists:last(Tokens)),
            case lists:nth(2, Tokens) of
                "true:" ->
                    Monkey#monkey{test_true = Value};
                "false:" ->
                    Monkey#monkey{test_false = Value}
            end
    end.

-spec new_monkey() -> monkey().
new_monkey() ->
    #monkey{}.

-spec process_line(io:device(), {non_neg_integer(), [monkey()]}) -> array:array(monkey()).
process_line(Device, {MonkeyIndex, Monkeys}) ->
    case io:get_line(Device, "") of
        eof ->
            array:fix(array:from_list(lists:reverse(Monkeys)));
        "\n" ->
            NewIndex = MonkeyIndex + 1,
            NewMonkey = new_monkey(),
            process_line(Device, {NewIndex, [NewMonkey|Monkeys]});
        Line ->
            [Monkey|Rest] = Monkeys,
            NewMonkey = update_monkey(string:trim(Line), Monkey),
            process_line(Device, {MonkeyIndex, [NewMonkey|Rest]})
    end.

-spec read_monkeys(string()) -> array:array(monkey()) | 'error'.
read_monkeys(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, {0, [new_monkey()]})};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec start_monkey_business(array:array(monkey()), non_neg_integer(), worry_fun()) -> inspect_counter().
start_monkey_business(Monkeys, Rounds, WorryFun) ->
    next_monkey_business_round(Monkeys, Rounds, maps:new(), WorryFun).

-spec next_monkey_business_round(array:array(monkey()), non_neg_integer(), inspect_counter(), worry_fun()) -> inspect_counter().
next_monkey_business_round(_, 0, InspectCounter, _) -> InspectCounter;
next_monkey_business_round(Monkeys, Rounds, InspectCounter, WorryFun) ->
    {NewMonkeys, NewInspectCounter} = do_monkey_business(Monkeys, InspectCounter, WorryFun),
    next_monkey_business_round(NewMonkeys, Rounds - 1, NewInspectCounter, WorryFun).

-spec do_monkey_business(array:array(monkey()), inspect_counter(), worry_fun()) -> {array:array(monkey()), inspect_counter()}.
do_monkey_business(Monkeys, InspectCounter, WorryFun) ->
    monkey_does_business(0, Monkeys, InspectCounter, WorryFun).

-spec monkey_does_business(non_neg_integer(), array:array(monkey()), inspect_counter(), worry_fun()) -> {array:array(monkey()), inspect_counter()}.
monkey_does_business(MonkeyIndex, Monkeys, InspectCounter, WorryFun) ->
    case MonkeyIndex >= array:size(Monkeys) of
        true ->
            {Monkeys, InspectCounter};
        false ->
            {NewMonkeys, NewInspectCounter} = monkey_inspect_items(MonkeyIndex, Monkeys, InspectCounter, WorryFun),
            monkey_does_business(MonkeyIndex + 1, NewMonkeys, NewInspectCounter, WorryFun)
    end.

-spec monkey_inspect_items(non_neg_integer(), array:array(monkey()), inspect_counter(), worry_fun()) -> {array:array(monkey()), inspect_counter()}.
monkey_inspect_items(MonkeyIndex, Monkeys, InspectCounter, WorryFun) ->
    Monkey = array:get(MonkeyIndex, Monkeys),
    case Monkey#monkey.items of
        [] ->
            {Monkeys, InspectCounter};
        [Item|Rest] ->
            {WorryLevel, TargetMonkeyIndex} = calc_worry_and_target_monkey(Monkey, Item, WorryFun),
            NewMonkey = Monkey#monkey{
                items = Rest
            },
            TargetMonkey = array:get(TargetMonkeyIndex, Monkeys),
            NewTargetMonkey = TargetMonkey#monkey{
                items = TargetMonkey#monkey.items ++ [WorryLevel]
            },
            NewMonkeys = array:set(MonkeyIndex, NewMonkey, array:set(TargetMonkeyIndex, NewTargetMonkey, Monkeys)),
            Val = maps:get(MonkeyIndex, InspectCounter, 0),
            NewInspectCounter = maps:put(MonkeyIndex, Val + 1, InspectCounter),
            monkey_inspect_items(MonkeyIndex, NewMonkeys, NewInspectCounter, WorryFun)
    end.


-spec calc_worry_and_target_monkey(monkey(), non_neg_integer(), worry_fun()) -> {non_neg_integer(), non_neg_integer()}.
calc_worry_and_target_monkey(Monkey, InitWorry, WorryFun) ->
    Fun = Monkey#monkey.op_fun,
    WorryLevel = Fun(InitWorry),
    NewWorryLevel = WorryFun(WorryLevel),
    case NewWorryLevel rem Monkey#monkey.test_div == 0 of
        true ->
            {NewWorryLevel, Monkey#monkey.test_true};
        false ->
            {NewWorryLevel, Monkey#monkey.test_false}
    end.

-spec calc_monkey_business(non_neg_integer(), inspect_counter()) -> non_neg_integer().
calc_monkey_business(CountActiveMonkeys, MonkeyBusiness) ->
    SortedActiveMonkeys = lists:sort(
        fun(A, B) ->
            A > B
        end, maps:values(MonkeyBusiness)),
    TopActiveMonkeys = lists:sublist(SortedActiveMonkeys, CountActiveMonkeys),
    lists:foldl(
        fun(El, Acc) ->
            Acc * El
        end, 1, TopActiveMonkeys).

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_monkeys(Filepath) of
        {ok, Monkeys} ->
            WorryFun = fun(X) ->
                X div 3
            end,
            MonkeyBusiness = start_monkey_business(Monkeys, 20, WorryFun),
            calc_monkey_business(2, MonkeyBusiness);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec calc_supermodulo(array:array(monkey())) -> non_neg_integer().
calc_supermodulo(Monkeys) ->
    array:foldl(
        fun(_, Monkey, Acc) ->
            Acc * Monkey#monkey.test_div
        end, 1, Monkeys).

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_monkeys(Filepath) of
        {ok, Monkeys} ->
            Supermodulo = calc_supermodulo(Monkeys),
            WorryFun = fun(X) ->
                X rem Supermodulo
            end,
            MonkeyBusiness = start_monkey_business(Monkeys, 10000, WorryFun),
            calc_monkey_business(2, MonkeyBusiness);
        error -> error
    end.