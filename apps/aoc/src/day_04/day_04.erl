-module(day_04).
-export([solve/2]).
-mode(compile).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec extract_sections(string()) -> {{non_neg_integer(), non_neg_integer()}, {non_neg_integer(), non_neg_integer()}}.
extract_sections(Line) ->
    [Section1, Section2] = string:split(Line, ","),
    { section_to_indices(Section1), section_to_indices(Section2) }.

-spec section_to_indices(string()) -> {non_neg_integer(), non_neg_integer()}.
section_to_indices(Section) ->
    [Low, High] = string:split(Section, "-"),
    {list_to_integer(Low), list_to_integer(High)}.
    
-spec check_overlaps_full({non_neg_integer(), non_neg_integer()}, {non_neg_integer(), non_neg_integer()}) -> boolean().
check_overlaps_full({S1Low, S1High}, {S2Low, S2High}) when S1Low =< S2Low, S1High >= S2High -> true;
check_overlaps_full({S1Low, S1High}, {S2Low, S2High}) when S2Low =< S1Low, S2High >= S1High -> true;
check_overlaps_full(_, _) -> false.

-spec check_line_overlapping(string(), fun(({non_neg_integer(), non_neg_integer()}, {non_neg_integer(), non_neg_integer()}) -> boolean())) -> boolean().
check_line_overlapping(Line, CheckFun) ->
    {Section1, Section2} = extract_sections(string:trim(Line)),
    CheckFun(Section1, Section2).

-spec process_line_1(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_1(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            NewAcc = case check_line_overlapping(string:trim(Line), fun check_overlaps_full/2) of
                true -> Acc + 1;
                false -> Acc
            end,
            process_line_1(Device, NewAcc)
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_1(Device, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec check_overlaps({non_neg_integer(), non_neg_integer()}, {non_neg_integer(), non_neg_integer()}) -> boolean().
check_overlaps({S1Low, _}, {S2Low, S2High}) when S1Low >= S2Low, S1Low =< S2High -> true;
check_overlaps({S1Low, S1High}, {S2Low, _}) when S2Low >= S1Low, S2Low =< S1High -> true;
check_overlaps(_, _) -> false.

-spec process_line_2(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_2(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            NewAcc = case check_line_overlapping(string:trim(Line), fun check_overlaps/2) of
                true -> Acc + 1;
                false -> Acc
            end,
            process_line_2(Device, NewAcc)
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            file:open(Filepath, [read]),
            process_line_2(Device, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.