-module(day_06).
-export([solve/2]).
-define(MARKER_START_OF_PACKET, 4).
-define(MARKER_START_OF_MESSAGE, 14).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec solve_line(string(), pos_integer()) -> non_neg_integer().
solve_line(Input, MarkerSize) when length(Input) =< MarkerSize -> 0;
solve_line(Input, MarkerSize) ->
    solve_next_line(Input, MarkerSize, MarkerSize).

-spec is_list_unique([any()]) -> boolean().
is_list_unique(List) ->
    case length(List) of
        0 -> false;
        N -> length(lists:uniq(List)) == N
    end.

-spec solve_next_line(string(), non_neg_integer(), pos_integer()) -> non_neg_integer().
solve_next_line([], _, _) -> 0;
solve_next_line(Input, MarkerSize, Index) ->
    Signal = lists:sublist(Input, MarkerSize),
    case is_list_unique(Signal) of
        true -> Index;
        false -> solve_next_line(tl(Input), MarkerSize, Index + 1)
    end.


-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            Line = io:get_line(Device, ""),
            solve_line(Line, ?MARKER_START_OF_PACKET);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            Line = io:get_line(Device, ""),
            solve_line(Line, ?MARKER_START_OF_MESSAGE);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.