-module(day_05).
-export([solve/2]).

-record(move_command, {count :: non_neg_integer(), from :: non_neg_integer(), to :: non_neg_integer()}).
-type move_command() :: #move_command{}.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec read_crate_content_line(non_neg_integer(), string(), map()) -> map().
read_crate_content_line(CrateNum, _, Acc) when CrateNum =< 0 -> Acc;
read_crate_content_line(CrateNum, Line, Acc) ->
    CharIndex = 1 + (CrateNum - 1) * 3 + CrateNum,
    NewAcc = case lists:nth(CharIndex, Line) of
        $ -> Acc;
        Char ->
            CratesOnNum = maps:get(CrateNum, Acc),
            maps:put(CrateNum, [Char|CratesOnNum], Acc)
    end,
    read_crate_content_line(CrateNum - 1, Line, NewAcc).

-spec parse_lines([string()], 'commands' | 'crate_numbers' | 'crates', {map:new(), []}) -> {map(), [move_command(), ...]}.
parse_lines([], _, Acc) -> Acc;
parse_lines([Line|Lines], _, Acc) when Line == "\n" -> parse_lines(Lines, crate_numbers, Acc);
parse_lines([Line|Lines], ReadPhase, {Crates, Commands}) ->
    case ReadPhase of
        crates ->
            CratesNum = length(maps:keys(Crates)),
            NewCrates = read_crate_content_line(CratesNum, Line, Crates),
            parse_lines(Lines, ReadPhase, {NewCrates, Commands});
        crate_numbers ->
            Nums = string:lexemes(string:trim(Line), " "),
            NewCrates = lists:foldl(
                fun(El, Acc) ->
                    maps:put(list_to_integer(El), [], Acc)
                end, maps:new(), Nums),
            parse_lines(Lines, crates, {NewCrates, Commands});
        commands ->
            [_, Count, _, From, _, To] = string:lexemes(string:trim(Line), " "),
            NewCommand = #move_command{
                count = list_to_integer(Count),
                from = list_to_integer(From),
                to = list_to_integer(To)
            },
            parse_lines(Lines, ReadPhase, {Crates, [NewCommand|Commands]})
    end.

-spec move_crates([move_command()], map(), 'crateMover9000' | 'crateMover9001') -> map().
move_crates([], Crates, _) -> Crates;
move_crates([Command|Commands], Crates, Model) ->
    CratesFrom = maps:get(Command#move_command.from, Crates),
    {Transfer, Remaining} = lists:split(Command#move_command.count, CratesFrom),
    CratesTo = maps:get(Command#move_command.to, Crates),
    NewCratesReduced = maps:put(Command#move_command.from, Remaining, Crates),
    NewListTo = case Model of
        crateMover9000 ->
            lists:reverse(Transfer) ++ CratesTo;
        crateMover9001 ->
            Transfer ++ CratesTo
    end,
    NewCrates = maps:put(Command#move_command.to, NewListTo, NewCratesReduced),
    move_crates(Commands, NewCrates, Model).

-spec look_top(map()) -> string().
look_top(Crates) ->
    Top = maps:fold(
        fun(_, V, Acc) ->
            case V of
                [] -> Acc;
                [El|_] -> [El|Acc]
            end
        end, [], Crates),
    lists:reverse(Top).

-spec read_file_and_solve(io:device(), 'crateMover9000' | 'crateMover9001') -> string().
read_file_and_solve(Device, Model) ->
    % Read lines reversed since it's much easier to make initial crate setup
    Lines = get_all_lines(Device, []),
    {Crates, Commands} = parse_lines(Lines, commands, {maps:new(), []}),
    NewCrates = move_crates(Commands, Crates, Model),
    look_top(NewCrates).

-spec get_all_lines(io:device(), []) -> [string(), ...].
get_all_lines(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line -> get_all_lines(Device, [Line|Acc])
    end.

-spec solve_1(string()) -> 'error' | string().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            read_file_and_solve(Device, crateMover9000);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec solve_2(string()) -> 'error' | string().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            read_file_and_solve(Device, crateMover9001);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.