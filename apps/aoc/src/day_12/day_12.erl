-module(day_12).
-export([solve/2]).

-type position() :: {non_neg_integer(), non_neg_integer()}.
-type direction() :: 'down' | 'left' | 'right' | 'up'.

-record(height_map, {
    matrix :: array:array(array:array(char())),
    size_x :: non_neg_integer(),
    size_y :: non_neg_integer(),
    start_pos :: position(),
    end_pos :: position()
}).
-type height_map() :: #height_map{}.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec list_matrix_to_array_matrix([non_neg_integer()]) -> array:array(non_neg_integer()).
list_matrix_to_array_matrix(ListMatrix) ->
    SizeY = length(ListMatrix),
    lists:foldl(
        fun({Index, Row}, Acc) ->
            array:set(Index, array:fix(array:from_list(Row)), Acc)
        end,
        array:new([{size, SizeY}, {fixed, true}]),
        lists:zip(lists:seq(0, SizeY - 1), ListMatrix)).

-spec process_line(io:device(), [string()]) -> height_map().
process_line(Device, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            Matrix = list_matrix_to_array_matrix(lists:reverse(Acc)),
            SizeX = length(hd(Acc)),
            SizeY = length(Acc),
            ReplaceSym = #{$S => $a, $E => $z},
            {NewMatrix, NewPosUpdate} = lists:foldl(
                fun(Index, {ItMatrix, ItPosUpdate}) ->
                    X = Index rem SizeX,
                    Y = Index div SizeX,
                    Char = array:get(X, array:get(Y, ItMatrix)),
                    case maps:get(Char, ReplaceSym, badkey) of
                        badkey ->
                            {ItMatrix, ItPosUpdate};
                        ReplChar ->
                            NewRow = array:set(X, ReplChar, array:get(Y, ItMatrix)),
                            NewItMatrix = array:set(Y, NewRow, ItMatrix),
                            NewItPosUpdate = maps:put(Char, {X, Y}, ItPosUpdate),
                            {NewItMatrix, NewItPosUpdate}
                    end
                end, {Matrix, maps:new()}, lists:seq(0, SizeX * SizeY - 1)),
            #height_map{
                matrix = NewMatrix,
                size_x = SizeX,
                size_y = SizeY,
                start_pos = maps:get($S, NewPosUpdate),
                end_pos = maps:get($E, NewPosUpdate)
            };
        "\n" ->
            process_line(Device, Acc);
        Line ->
            process_line(Device, [string:trim(Line)|Acc])
    end.

-spec read_height_map(string()) -> 'error' | {'ok', height_map()}.
read_height_map(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec add_to_pos(position(), position()) -> position().
add_to_pos({Pos1X, Pos1Y}, {Pos2X, Pos2Y}) ->
    {Pos1X + Pos2X, Pos1Y + Pos2Y}.

-spec get_height(position(), height_map()) -> non_neg_integer().
get_height({PosX, PosY}, HeightMap) ->
    array:get(PosX, array:get(PosY, HeightMap#height_map.matrix)).

-spec calc_move_target({number(), number()}, direction()) -> {number(), number()}.
calc_move_target(From, Direction) ->
    case Direction of
        up ->
            add_to_pos(From, { 0, -1});
        down ->
            add_to_pos(From, { 0,  1});
        left ->
            add_to_pos(From, {-1,  0});
        right ->
            add_to_pos(From, { 1,  0})
    end.

-spec can_move_to(position(), position(), height_map()) -> boolean().
can_move_to(_, {ToX, _}, _) when ToX < 0 -> false;
can_move_to(_, {ToX, _}, HeightMap) when ToX >= HeightMap#height_map.size_x -> false;
can_move_to(_, {_, ToY}, _) when ToY < 0 -> false;
can_move_to(_, {_, ToY}, HeightMap) when ToY >= HeightMap#height_map.size_y -> false;
can_move_to(From, To, HeightMap) ->
    FromHeight = get_height(From, HeightMap),
    ToHeight = get_height(To, HeightMap),
    (ToHeight - FromHeight) =< 1.

-spec find_shortest_path(height_map()) -> 'failure' | {'ok', height_map()}.
find_shortest_path(HeightMap) ->
    OpenSet = pqueue:in(HeightMap#height_map.start_pos, pqueue:new()),
    GScore = maps:put(HeightMap#height_map.start_pos, 0, maps:new()),
    FScore = maps:put(HeightMap#height_map.start_pos, calc_h(HeightMap#height_map.start_pos, HeightMap), maps:new()),
    a_star(OpenSet, maps:new(), GScore, FScore, HeightMap).

-spec calc_h(position(), height_map()) -> number().
calc_h(_Pos, _HeightMap) ->
    1.

-spec calc_d(position(), position()) -> number().
calc_d(_A, _B) ->
    1.

-spec reconstruct_path(map(), position(), [position()]) -> [position()].
reconstruct_path(CameFrom, Current, Acc) ->
    case maps:get(Current, CameFrom, badkey) of
        badkey ->
            Acc;
        Next ->
            reconstruct_path(CameFrom, Next, [Current|Acc])
    end.

-spec a_star(pqueue:pqueue(), maps:map(), maps:map(), maps:map(), height_map()) -> 'failure' | {'ok', [position()]}.
a_star(OpenSet, CameFrom, GScore, FScore, HeightMap) ->
    case pqueue:out(OpenSet) of
        {empty, _} ->
            failure;
        {{value, Current}, ResOpenSet} ->
            case Current == HeightMap#height_map.end_pos of
                true ->
                    {ok, reconstruct_path(CameFrom, Current, [])};
                false ->
                    Neighbors = get_accessible_neighbors(Current, HeightMap),
                    {NewOpenSet, NewCameFrom, NewGScore, NewFScore} = loop_neighbor(Current, Neighbors, HeightMap, {ResOpenSet, CameFrom, GScore, FScore}),
                    a_star(NewOpenSet, NewCameFrom, NewGScore, NewFScore, HeightMap)
            end
    end.

-spec loop_neighbor(position(), [position()], height_map(), {pqueue:pqueue(), maps:map(), maps:map(), maps:map()}) -> {pqueue:pqueue(), maps:map(), maps:map(), maps:map()}.
loop_neighbor(_, [], _, {OpenSet, CameFrom, GScore, FScore}) ->
    {OpenSet, CameFrom, GScore, FScore};
loop_neighbor(Current, [Neighbor|Rest], HeightMap, {OpenSet, CameFrom, GScore, FScore}) ->
    TentativeScore = maps:get(Current, GScore, infinity) + calc_d(Current, Neighbor),
    GScoreNeighbor = maps:get(Neighbor, GScore, infinity),
    case (GScoreNeighbor == infinity) or (TentativeScore < GScoreNeighbor) of
        false ->
            loop_neighbor(Current, Rest, HeightMap, {OpenSet, CameFrom, GScore, FScore});
        true ->
            NewCameFrom = maps:put(Neighbor, Current, CameFrom),
            NewGScore = maps:put(Neighbor, TentativeScore, GScore),
            NewFScore = maps:put(Neighbor, TentativeScore + calc_h(Neighbor, HeightMap), FScore),
            NewOpenSet = pqueue:in(Neighbor, OpenSet),
            loop_neighbor(Current, Rest, HeightMap, {NewOpenSet, NewCameFrom, NewGScore, NewFScore})
    end.

-spec get_accessible_neighbors(position(), height_map()) -> [position()].
get_accessible_neighbors(Pos, HeightMap) ->
    lists:foldl(
        fun(Direction, Acc) ->
            Target = calc_move_target(Pos, Direction),
            case can_move_to(Pos, Target, HeightMap) of
                true ->
                    [Target|Acc];
                false ->
                    Acc
            end
        end, [], [up, down, left, right]).

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_height_map(Filepath) of
        {ok, HeightMap} ->
            case find_shortest_path(HeightMap) of
                {ok, Path} ->
                    length(Path);
                failure -> error
            end;
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec find_all_position_of_elevation(non_neg_integer(), height_map()) -> [position()].
find_all_position_of_elevation(Type, HeightMap) ->
    array:foldl(
        fun(Y, Row, Acc) ->
            array:foldl(
                fun(X, Val, RowAcc) ->
                    case Val == Type of
                        true ->
                            [{X, Y}|RowAcc];
                        false ->
                            RowAcc
                    end
                end, Acc, Row)
        end, [], HeightMap#height_map.matrix).

-spec solve_for_starting_points([position()], height_map()) -> [[position()]].
solve_for_starting_points(StartingPoints, HeightMap) ->
    solve_for_starting_points_it(StartingPoints, HeightMap, []).

-spec solve_for_starting_points_it([position()], height_map(), [[position()]]) -> [[position()]].
solve_for_starting_points_it([], _, Acc) ->
    lists:reverse(Acc);
solve_for_starting_points_it([StartPos|Rest], HeightMap, Acc) ->
    NewHeightMap = HeightMap#height_map{start_pos = StartPos},
    NewAcc = case find_shortest_path(NewHeightMap) of
        {ok, Path} ->
            [Path|Acc];
        failure ->
            Acc
    end,
    solve_for_starting_points_it(Rest, HeightMap, NewAcc).

-spec find_best_solution([[position()]]) -> non_neg_integer().
find_best_solution(Solutions) ->
    Counts = lists:map(
        fun(Path) ->
            length(Path)
        end, Solutions),
    lists:min(Counts).

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_height_map(Filepath) of
        {ok, HeightMap} ->
            StartingPoints = find_all_position_of_elevation($a, HeightMap),
            Solutions = solve_for_starting_points(StartingPoints, HeightMap),
            find_best_solution(Solutions);
        error -> error
    end.