-module(day_14).
-export([solve/2]).
-define(INFINITE_LINE_ADD, 2).
-define(RANGE_EXTEND, 50).

-type position() :: {integer(), integer()}.
-type cell_fill() :: 'air' | 'sand' | 'solid'.

-record(cave, {
    matrix :: array:array(array:array(cell_fill())),
    range_x :: {integer(), integer()},
    range_y :: {integer(), integer()},
    sand_entry_point :: position(),
    infinite_floor_line :: 'none' | integer()
}).
-type cave() :: #cave{}.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec parse_line(string()) -> {position()}.
parse_line(Line) ->
    lists:map(
        fun(PosStr) ->
            [X, Y] = string:tokens(PosStr, ","),
            {list_to_integer(X), list_to_integer(Y)}
        end,
        string:tokens(Line, " -> ")).

-spec process_line(io:device(), [position()]) -> [position()].
process_line(Device, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            lists:reverse(Acc);
        Line ->
            Instruction = parse_line(string:trim(Line)),
            process_line(Device, [Instruction|Acc])
    end.

-spec read_cave_instructions(string()) -> [position()].
read_cave_instructions(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec construct_cave_matrix([[position()]], position(), position()) -> array:array(array:array(position())).
construct_cave_matrix(Instructions, {RangeXMin, RangeXMax}, {RangeYMin, RangeYMax}) ->
    SizeX = RangeXMax - RangeXMin + 1,
    SizeY = RangeYMax - RangeYMin + 1,
    EmptyMatrix = array:map(
        fun(_Index, _Row) ->
            array:new([{size, SizeX}, {fixed, true}, {default, air}])
        end, array:new([{size, SizeY}, {fixed, true}, {default, air}])),
    construct_cave_matrix_it(Instructions, {RangeXMin, RangeXMax}, {RangeYMin, RangeYMax}, EmptyMatrix).

-spec construct_cave_matrix_it([[position()]], position(), position(), array:array(array:array(position()))) -> array:array(array:array(position)).
construct_cave_matrix_it([], _, _, Matrix) -> Matrix;
construct_cave_matrix_it([Instruction|Rest], {RangeXMin, RangeXMax}, {RangeYMin, RangeYMax}, Matrix) ->
    Pairs = lists:foldl(
        fun(Index, Acc) ->
            El1 = lists:nth(Index - 1, Instruction),
            El2 = lists:nth(Index, Instruction),
            [{El1, El2}|Acc]
        end,
        [], lists:seq(2, length(Instruction))),
    NewMatrix = lists:foldl(
        fun({Point1, Point2}, ItMatrix) ->
            Line = generate_line(Point1, Point2),
            lists:foldl(
                fun({OffX, OffY}, Acc) ->
                    X = OffX - RangeXMin,
                    Y = OffY - RangeYMin,
                    NewRow = array:set(X, solid, array:get(Y, Acc)),
                    array:set(Y, NewRow, Acc)
                end, ItMatrix, Line)
        end, Matrix, Pairs),
    construct_cave_matrix_it(Rest, {RangeXMin, RangeXMax}, {RangeYMin, RangeYMax}, NewMatrix).

-spec get_direction(non_neg_integer(), non_neg_integer()) -> -1 | 0 | 1.
get_direction(Val1, Val2) when Val1 > Val2 -> -1;
get_direction(Val1, Val2) when Val1 < Val2 -> 1;
get_direction(_, _) -> 0.

-spec generate_line(position(), position()) -> [position()].
generate_line({X1, Y1}, {X2, Y2}) when X1 == X2 ->
    [{X1, V} || V <- lists:seq(Y1, Y2, get_direction(Y1, Y2))];
generate_line({X1, Y1}, {X2, Y2}) when Y1 == Y2 ->
    [{V, Y1} || V <- lists:seq(X1, X2, get_direction(X1, X2))].

-spec construct_cave([[position()]], position(), integer()) -> cave().
construct_cave(Instructions, SandEntryPoint, InfiniteFloorLine) ->
    {RangeX, RangeY} = find_minmax(Instructions, SandEntryPoint),
    NewRangeY = add_to_pos(RangeY, {0, ?INFINITE_LINE_ADD - 1}),
    Matrix = construct_cave_matrix(Instructions, RangeX, NewRangeY),
    #cave{
        matrix = Matrix,
        range_x = RangeX,
        range_y = NewRangeY,
        sand_entry_point = SandEntryPoint,
        infinite_floor_line = InfiniteFloorLine
    }.

-spec find_minmax([[position()]], position()) -> {position(), position()}.
find_minmax(Instructions, SandEntryPoint) ->
    CompareFun = fun(Val, Cmp, AccVal) ->
        case AccVal of
            undef -> Val;
            _ ->
                case Cmp of
                    greater ->
                        case Val > AccVal of
                            true -> Val;
                            false -> AccVal
                        end;
                    less ->
                        case Val < AccVal of
                            true -> Val;
                            false -> AccVal
                        end
                end
        end
    end,
    {SandEntryPointX, SandEntryPointY} = SandEntryPoint,
    lists:foldl(
        fun(Instruction, Acc) ->
            lists:foldl(
                fun({X, Y}, {{MinX, MaxX}, {MinY, MaxY}}) ->
                    {
                        {CompareFun(X, less, MinX), CompareFun(X, greater, MaxX)},
                        {CompareFun(Y, less, MinY), CompareFun(Y, greater, MaxY)}
                    }
                end, Acc, Instruction)
        end, {{SandEntryPointX, SandEntryPointX}, {SandEntryPointY, SandEntryPointY}}, Instructions).

-spec get_value_at(position(), cave()) -> invalid_pos | cell_fill().
get_value_at({PosX, _}, _) when PosX < 0 -> invalid_pos;
get_value_at({_, PosY}, _) when PosY < 0 -> invalid_pos;
get_value_at({PosX, PosY}, Cave) ->
    {RangeXMin, RangeXMax} = Cave#cave.range_x,
    {RangeYMin, RangeYMax} = Cave#cave.range_y,
    case (PosX > RangeXMax) or (PosY > RangeYMax) of
        true ->
            invalid_pos;
        false ->
            Row = array:get(PosY - RangeYMin, Cave#cave.matrix),
            array:get(PosX - RangeXMin, Row)
    end.

-spec set_value_at(position(), cell_fill(), cave()) -> cave().
set_value_at({PosX, PosY}, Type, Cave) ->
    {RangeXMin, _} = Cave#cave.range_x,
    {RangeYMin, _} = Cave#cave.range_y,
    X = PosX - RangeXMin,
    Y = PosY - RangeYMin,
    Row = array:set(X, Type, array:get(Y, Cave#cave.matrix)),
    Matrix = array:set(Y, Row, Cave#cave.matrix),
    Cave#cave{matrix = Matrix}.

-spec start_simulation(cave()) -> non_neg_integer().
start_simulation(Cave) ->
    simulate_sand(Cave, 0).

-spec simulate_sand(cave(), non_neg_integer()) -> non_neg_integer().
simulate_sand(Cave, Count) ->
    SandEntryPoint = Cave#cave.sand_entry_point,
    case get_value_at(SandEntryPoint, Cave) of
        air ->
            NewCave = set_value_at(SandEntryPoint, sand, Cave),
            case next_step(SandEntryPoint, NewCave) of
                {abyss, _} -> Count;
                {done, ResultCave} ->
                    simulate_sand(ResultCave, Count + 1)
            end;
        _ ->
            Count
    end.

-spec add_to_pos(position(), position()) -> position().
add_to_pos({Pos1X, Pos1Y}, {Pos2X, Pos2Y}) ->
    {Pos1X + Pos2X, Pos1Y + Pos2Y}.

-spec is_in_abyss(position(), cave()) -> boolean().
is_in_abyss({_, PosY}, Cave) when Cave#cave.infinite_floor_line == none ->
    {_, MaxY} = Cave#cave.range_y,
    PosY >= MaxY;
is_in_abyss(_, _) -> false.

-spec extend_array_and_shift(array:array(any()), non_neg_integer()) -> array:array(any()).
extend_array_and_shift(Array, ExtendCount) ->
    OldSize = array:size(Array),
    NewSize = OldSize + 2 * ExtendCount,
    ExtendedArray = array:resize(NewSize, Array),
    lists:foldl(
        fun(Index, Acc) ->
            PrevIndex = Index - ExtendCount,
            OldVal = array:get(Index, Acc),
            NewAcc = array:set(Index, array:get(PrevIndex, Acc), Acc),
            array:set(PrevIndex, OldVal, NewAcc)
        end, ExtendedArray, lists:seq(OldSize + ExtendCount, ExtendCount, -1)).

-spec extend_cave_if_needed(position(), cave(), pos_integer()) -> cave().
extend_cave_if_needed({ToX, _}, Cave, ExtendCount) ->
    {MinX, MaxX} = Cave#cave.range_x,
    case (ToX < MinX) or (ToX > MaxX) of
        true ->
            NewMatrix = array:map(
                fun(_Y, Row) ->
                    extend_array_and_shift(Row, ExtendCount)
                end, Cave#cave.matrix),
            NewRangeX = add_to_pos(Cave#cave.range_x, {-ExtendCount, ExtendCount}),
            NewCave = Cave#cave{
                matrix = NewMatrix,
                range_x = NewRangeX
            },
            NewCave;
        false ->
            Cave
    end.

-spec try_move_sand_to(position(), position(), cave()) -> {false, cave()} | {true, cave()}.
try_move_sand_to(_, {_, ToY}, Cave) when ToY == Cave#cave.infinite_floor_line -> {false, Cave};
try_move_sand_to(From, To, Cave) ->
    ExtendedCave = extend_cave_if_needed(To, Cave, ?RANGE_EXTEND),
    case get_value_at(To, ExtendedCave) of
        air ->
            NewCave = set_value_at(From, air, ExtendedCave),
            {true, set_value_at(To, sand, NewCave)};
        invalid_pos ->
            {false, ExtendedCave};
        _ ->
            {false, ExtendedCave}
    end.

-spec next_step(position(), cave()) -> {abyss, cave()} | {done, cave()}.
next_step(From, Cave) ->
    BelowPos = add_to_pos(From, {0, 1}),
    case is_in_abyss(BelowPos, Cave) of
        true ->
            {abyss, Cave};
        false ->
            BelowLeft = add_to_pos(From, {-1, 1}),
            BelowRight = add_to_pos(From, {1, 1}),
            NextSteps = [BelowPos, BelowLeft, BelowRight],
            case try_steps(From, NextSteps, Cave) of
                {done, NewCave} ->
                    {done, NewCave};
                {NextPos, NewCave} ->
                    next_step(NextPos, NewCave)
            end
    end.

-spec try_steps(position(), [position()], cave()) -> {true, cave()} | {false, cave()}.
try_steps(_, [], Cave) ->
    {done, Cave};
try_steps(From, [To|Rest], Cave) ->
    case try_move_sand_to(From, To, Cave) of
        {true, NewCave} ->
            {To, NewCave};
        {false, NewCave} ->
            try_steps(From, Rest, NewCave)
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_cave_instructions(Filepath) of
        {ok, Instructions} ->
            SandEntryPoint = {500, 0},
            Cave = construct_cave(Instructions, SandEntryPoint, none),
            start_simulation(Cave);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec calc_infinite_floor_line([[position()]], position()) -> integer().
calc_infinite_floor_line(Instructions, SandEntryPoint) ->
    {{_, _}, {_, RangeYMax}} = find_minmax(Instructions, SandEntryPoint),
    RangeYMax + ?INFINITE_LINE_ADD.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_cave_instructions(Filepath) of
        {ok, Instructions} ->
            SandEntryPoint = {500, 0},
            InfiniteFloorLine = calc_infinite_floor_line(Instructions, SandEntryPoint),
            Cave = construct_cave(Instructions, SandEntryPoint, InfiniteFloorLine),
            start_simulation(Cave);
        error -> error
    end.