-module(aoc_app).

-export([solve_all/0, solve/1]).

get_days_path() ->
    {_, CurrentDirectory} = file:get_cwd(),
    filename:join([CurrentDirectory, "apps", "aoc", "src"]).

solve_all() ->
    DaysDirPath = get_days_path(),
    {_, AllDirs} = file:list_dir(DaysDirPath),
    lists:map(
        fun(DirStr) ->
            case (filelib:is_dir(filename:join(DaysDirPath, DirStr))) and (string:substr(DirStr, 1, 3) == "day") of
                true ->
                    io:format("Solving ~s~n", [DirStr]),
                    solve(list_to_atom(DirStr)),
                    io:format("=================~n");
                false ->
                    ok
            end
        end, AllDirs).

solve(Day) ->
    DayStr = atom_to_list(Day),
    DaysPath = get_days_path(),
    File1 = filename:join([DaysPath, DayStr, "input.txt"]),
    File2 = filename:join([DaysPath, DayStr, "input.txt"]),
    Day:solve(File1, File2).
