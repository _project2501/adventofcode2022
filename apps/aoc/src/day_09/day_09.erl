-module(day_09).
-export([solve/2]).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

-type position() :: {integer(), integer()}.
-type direction() :: 'up' | 'down' | 'left' | 'right'.

-record(rope_state, {
    knots :: array:array(position()),
    visited :: map()}).
-type rope_state() :: #rope_state{}.

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec add_to_pos(position(), position()) -> position().
add_to_pos({PosX, PosY}, {DeltaX, DeltaY}) ->
    {PosX + DeltaX, PosY + DeltaY}.

-spec sub_from_pos(position(), position()) -> position().
sub_from_pos({PosX, PosY}, {DeltaX, DeltaY}) ->
    {PosX - DeltaX, PosY - DeltaY}.

-spec is_two_cells_appart(position(), position()) -> boolean().
is_two_cells_appart(HeadPos, TailPos) ->
    {DiffX, DiffY} = sub_from_pos(HeadPos, TailPos),
    case {abs(DiffX), abs(DiffY)} of
        {2, _} -> true;
        {_, 2} -> true;
        _ -> false
    end.

-spec apply_command_n_times(direction(), non_neg_integer(), rope_state()) -> rope_state().
apply_command_n_times(_, 0, State) -> State;
apply_command_n_times(Direction, Count, State) ->
    MovedRopeState = move_rope(Direction, State),
    NewState = inc_tail_visited_count(MovedRopeState),
    apply_command_n_times(Direction, Count - 1, NewState).

-spec calculate_move_delta(direction()) -> position().
calculate_move_delta(up) ->      { 0,  1 };
calculate_move_delta(down) ->    { 0, -1 };
calculate_move_delta(left) ->    {-1,  0 };
calculate_move_delta(right) ->   { 1,  0 }.

-spec move_rope(direction(), rope_state()) -> rope_state().
move_rope(Direction, State) ->
    HeadIndex = 0,
    OldHeadPos = array:get(HeadIndex, State#rope_state.knots),
    NewHeadPos = move_knot(OldHeadPos, Direction),
    NewState = update_knot_position(HeadIndex, NewHeadPos, State),
    move_knots(HeadIndex + 1, NewState).

-spec move_knot(position(), direction()) -> position().
move_knot(Pos, Direction) ->
    MoveDelta = calculate_move_delta(Direction),
    add_to_pos(Pos, MoveDelta).

-spec move_knots(pos_integer(), rope_state()) -> rope_state().
move_knots(KnotIndex, State) ->
    case KnotIndex >= array:size(State#rope_state.knots) of
        true -> State;
        false ->
            HeadPos = array:get(KnotIndex - 1, State#rope_state.knots),
            TailPos = array:get(KnotIndex, State#rope_state.knots),
            NewTailPos = calculate_tail_pos(HeadPos, TailPos),
            NewState = update_knot_position(KnotIndex, NewTailPos, State),
            move_knots(KnotIndex + 1, NewState)
    end.

-spec sign(number()) -> -1 | 0 | 1.
sign(0) -> 0;
sign(N) when N < 0 -> -1;
sign(N) when N > 0 -> 1.

-spec calculate_tail_pos(position(), position()) -> position().
calculate_tail_pos(HeadPos, TailPos) ->
    case is_two_cells_appart(HeadPos, TailPos) of
        false -> TailPos;
        true ->
            {DiffX, DiffY} = sub_from_pos(HeadPos, TailPos),
            MoveDelta = case {DiffX, DiffY} of
                % Moved horizontally
                {_, 0} ->
                    {sign(DiffX), 0};
                % Moved vertically
                {0, _} ->
                    {0, sign(DiffY)};
                % Moved diagonally
                _ ->
                    {sign(DiffX), sign(DiffY)}
            end,
            add_to_pos(TailPos, MoveDelta)
    end.

-spec update_knot_position(pos_integer(), position(), rope_state()) -> rope_state().
update_knot_position(KnotIndex, Pos, State) ->
    State#rope_state {
        knots = array:set(KnotIndex, Pos, State#rope_state.knots)
    }.

-spec get_command(string()) -> {direction(), non_neg_integer()}.
get_command(Line) ->
    Tokens = string:tokens(Line, " "),
    Count = list_to_integer(hd(tl(Tokens))),
    case hd(Tokens) of
        "U" -> {up, Count};
        "D" -> {down, Count};
        "L" -> {left, Count};
        "R" -> {right, Count}
    end.

-spec count_visited_places(rope_state()) -> non_neg_integer().
count_visited_places(State) ->
    maps:size(State#rope_state.visited).

-spec inc_tail_visited_count(rope_state()) -> rope_state().
inc_tail_visited_count(State) ->
    LastIndex = array:size(State#rope_state.knots) - 1,
    TailPos = array:get(LastIndex, State#rope_state.knots),
    inc_visited_count(TailPos, State).

-spec inc_visited_count(position(), rope_state()) -> rope_state().
inc_visited_count(Pos, State) ->
    Val = maps:get(Pos, State#rope_state.visited, 0),
    State#rope_state{
        visited = maps:put(Pos, Val + 1, State#rope_state.visited)
    }.

-spec init_knots(pos_integer()) -> array:array(position()).
init_knots(KnotCount) ->
    array:new([{size, KnotCount}, {fixed, true}, {default, {0, 0}}]).

-spec new_state(pos_integer()) -> rope_state().
new_state(KnotCount) ->
    State = #rope_state{
        knots = init_knots(KnotCount),
        visited = maps:new()
    },
    LastIndex = array:size(State#rope_state.knots) - 1,
    inc_visited_count(array:get(LastIndex, State#rope_state.knots), State).

-spec process_line_1(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_1(Device, State) ->
    case io:get_line(Device, "") of
        eof -> count_visited_places(State);
        Line ->
            {Direction, Count} = get_command(string:trim(Line)),
            NewState = apply_command_n_times(Direction, Count, State),
            process_line_1(Device, NewState)
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_1(Device, new_state(2));
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec process_line_2(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_2(Device, State) ->
    case io:get_line(Device, "") of
        eof -> count_visited_places(State);
        Line ->
            {Direction, Count} = get_command(string:trim(Line)),
            NewState = apply_command_n_times(Direction, Count, State),
            process_line_2(Device, NewState)
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_2(Device, new_state(10));
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.