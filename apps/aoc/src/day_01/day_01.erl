-module(day_01).
-export([solve/2]).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec choose_bigger(non_neg_integer(), non_neg_integer()) -> non_neg_integer().
choose_bigger(Val1, Val2) when Val1 > Val2 -> Val1;
choose_bigger(_, Val) -> Val.

-spec process_line_1(io:device(), non_neg_integer(), non_neg_integer()) -> non_neg_integer().
process_line_1(Device, Current, Acc) ->
    case io:get_line(Device, "") of
        eof -> choose_bigger(Current, Acc);
        "\n" ->
            NewAcc = choose_bigger(Current, Acc),
            process_line_1(Device, 0, NewAcc);
        Line ->
            Value = list_to_integer(string:trim(Line)),
            process_line_1(Device, Current + Value, Acc)
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_1(Device, 0, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec choose_top3(non_neg_integer(), [non_neg_integer()]) -> [non_neg_integer()].
choose_top3(Val, [Elem1, Elem2, _])     when Val > Elem1 -> [Val, Elem1, Elem2];
choose_top3(Val, [Elem1, Elem2, _])     when Val > Elem2 -> [Elem1, Val, Elem2];
choose_top3(Val, [Elem1, Elem2, Elem3]) when Val > Elem3 -> [Elem1, Elem2, Val];
choose_top3(_, Top3) -> Top3.

-spec process_line_2(io:device(), non_neg_integer(), [non_neg_integer()]) -> non_neg_integer().
process_line_2(Device, Current, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            NewAcc = choose_top3(Current, Acc),
            lists:sum(NewAcc);
        "\n" ->
            NewAcc = choose_top3(Current, Acc),
            process_line_2(Device, 0, NewAcc);
        Line ->
            Value = list_to_integer(string:trim(Line)),
            process_line_2(Device, Current + Value, Acc)
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            file:open(Filepath, [read]),
            process_line_2(Device, 0, [0, 0, 0]);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.