-module(day_03).
-export([solve/2]).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec halve_line(string()) -> {string(), string()}.
halve_line(Line) ->
    Len = length(Line),
    lists:split(Len div 2, Line).

-spec find_same2(string(), string()) -> sets:set(char()).
find_same2(List1, List2) ->
    lists:foldl(
        fun(El, Acc) ->
            case lists:member(El, List2) of
                true -> sets:add_element(El, Acc);
                false -> Acc
            end
        end, sets:new(), List1).

% A-Z (65-90)
-spec eval_item(char()) -> non_neg_integer().
eval_item(Item) when Item >= $A, Item =< $Z -> Item - $A + 27;

% a-z (97-122)
eval_item(Item) when Item >= $a, Item =< $z -> Item - $a + 1.

-spec eval_items_set(sets:set(char())) -> non_neg_integer().
eval_items_set(Items) ->
    sets:fold(
        fun(El, Acc) ->
            Acc + eval_item(El)
        end, 0, Items).

-spec process_line_1(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_1(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            {FirstHalf, SecondHalf} = halve_line(string:trim(Line)),
            SameItems = find_same2(FirstHalf, SecondHalf),
            process_line_1(Device, Acc + eval_items_set(SameItems))
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_1(Device, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec find_same_triplet([string()]) -> sets:set(char()).
find_same_triplet([List1, List2, List3]) ->
    Set1 = find_same2(List1, List2),
    Set2 = find_same2(List2, List3),
    sets:intersection(Set1, Set2).

-spec process_line_2(io:device(), string(), non_neg_integer()) -> non_neg_integer().
process_line_2(Device, Triplet, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            Inventory = string:trim(Line),
            case length(Triplet) == 2 of
                true ->
                    Item = find_same_triplet([Inventory|Triplet]),
                    NewAcc = Acc + eval_items_set(Item),
                    process_line_2(Device, [], NewAcc);
                false ->
                    NewTriplet = [Inventory|Triplet],
                    process_line_2(Device, NewTriplet, Acc)
            end
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            file:open(Filepath, [read]),
            process_line_2(Device, [], 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.