-module(day_15).
-export([solve/2]).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

-type position() :: {integer(), integer()}.

-record(sensor, {
    at :: position(),
    beacon :: position(),
    beacon_distance :: pos_integer()
}).
-type sensor() :: #sensor{}.

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec parse_line(string()) -> sensor().
parse_line(Line) ->
    Tokens = string:tokens(Line, " "),
    ParseXyValues = fun(Start) ->
        list_to_tuple(
            lists:map(
                fun(StrUntrimmed) ->
                    Str = string:trim(string:trim(StrUntrimmed, both, ","), both, ":"),
                    [_, ValStr] = string:tokens(Str, "="),
                    list_to_integer(ValStr)
                end, lists:sublist(Tokens, Start, 2)))
    end,
    At = ParseXyValues(3),
    Beacon = ParseXyValues(9),
    #sensor{
        at = At,
        beacon = Beacon,
        beacon_distance = distance(At, Beacon)
    }.

-spec process_line(io:device(), [sensor()]) -> [sensor()].
process_line(Device, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            lists:reverse(Acc);
        Line ->
            SensorLine = parse_line(string:trim(Line)),
            process_line(Device, [SensorLine|Acc])
    end.

-spec read_sensor(string()) -> 'error' | {'ok', [sensor()]}.
read_sensor(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec distance(position(), position()) -> integer().
distance({FromX, FromY}, {ToX, ToY}) ->
    abs(FromX - ToX) + abs(FromY - ToY).

-spec get_sensor_square_range(sensor()) -> {position(), position()}.
get_sensor_square_range(Sensor) ->
    SensorRange = Sensor#sensor.beacon_distance,
    {X, Y} = Sensor#sensor.at,
    {{X - SensorRange, Y - SensorRange}, {X + SensorRange, Y + SensorRange}}.

-spec count_invalid_positions_on_line([sensor()], integer()) -> non_neg_integer().
count_invalid_positions_on_line(Sensors, LineY) ->
    SensorRanges = gather_sensor_ranges_on_line(Sensors, LineY, []),
    JoinedRanges = join_all_ranges(SensorRanges),
    sum_ranges(JoinedRanges).

-spec gather_sensor_ranges_on_line([sensor()], non_neg_integer(), [{integer(), integer()}]) -> [{integer(), integer()}].
gather_sensor_ranges_on_line([], _, Acc) ->
    lists:reverse(Acc);
gather_sensor_ranges_on_line([Sensor|Rest], LineY, Acc) ->
    NewAcc = case get_overlap_range_with_line(Sensor, LineY) of
        none -> Acc;
        Range -> [Range|Acc]
    end,
    gather_sensor_ranges_on_line(Rest, LineY, NewAcc).

-spec get_overlap_range_with_line(sensor(), non_neg_integer()) -> 'none' | {integer(), integer()}.
get_overlap_range_with_line(Sensor, LineY) ->
    {{_, YMin}, {_, YMax}} = get_sensor_square_range(Sensor),
    case (LineY >= YMin) and (LineY =< YMax) of
        true ->
            {SensorPosX, SensorPosY} = Sensor#sensor.at,
            Diff = abs(SensorPosY - LineY),
            Range = Sensor#sensor.beacon_distance,
            StartX = SensorPosX - Range + Diff,
            EndX = SensorPosX + Range - Diff,
            {StartX, EndX};
        false -> none
    end.

-spec sum_ranges([{integer(), integer()}]) -> integer().
sum_ranges(SensorRanges) ->
    lists:foldl(
        fun({MinX, MaxX}, Acc) ->
            Acc + MaxX - MinX
        end, 0, SensorRanges).

-spec join_all_ranges([{integer(), integer()}]) -> [{integer(), integer()}].
join_all_ranges([]) -> [];
join_all_ranges([X]) -> [X];
join_all_ranges(Ranges) ->
    Sorted = lists:sort(Ranges),
    lists:reverse(join_all_ranges_it(tl(Sorted), [hd(Sorted)])).

-spec join_all_ranges_it([{integer(), integer()}], [{integer(), integer()}, ...]) -> [{integer(), integer()}, ...].
join_all_ranges_it([], Acc) -> Acc;
join_all_ranges_it([Elem2|Rest], Acc) ->
    Elem1 = hd(Acc),
    case join_ranges(Elem1, Elem2) of
        false ->
            join_all_ranges_it(Rest, [Elem2|Acc]);
        {true, Joined} ->
            join_all_ranges_it(Rest, [Joined|tl(Acc)])
    end.
    
-spec join_ranges({integer(), integer()}, {integer(), integer()}) -> 'false' | {'true', {integer(), integer()}}.
join_ranges({AStartX, X}, {X, BEndX}) -> {true, {AStartX, BEndX}};
join_ranges({AStartX, AEndX}, {BStartX, BEndX}) when AEndX > BStartX -> {true, {AStartX, max(AEndX, BEndX)}};
join_ranges(_, _) -> false.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_sensor(Filepath) of
        {ok, Sensors} ->
            LineY = 2000000,
            count_invalid_positions_on_line(Sensors, LineY);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec add_to_pos(position(), position()) -> position().
add_to_pos({PosX, PosY}, {DeltaX, DeltaY}) ->
    {PosX + DeltaX, PosY + DeltaY}.

-spec find_distress_signal([sensor()], {integer(), integer()}, integer()) -> integer().
find_distress_signal(Sensors, Range, Multiplier) ->
    check_all_sensors(Sensors, Sensors, Range, Multiplier).

-spec check_all_sensors([sensor()], [sensor()], {integer(), integer()}, integer()) -> integer().
check_all_sensors([], _, _, _) -> 0;
check_all_sensors([Sensor|Rest], AllSensors, Range, Multiplier) ->
    case check_sensor(Sensor, lists:delete(Sensor, AllSensors), Range) of
        {true, {PosX, PosY}} -> PosX * Multiplier + PosY;
        false -> check_all_sensors(Rest, AllSensors, Range, Multiplier)
    end.

-spec is_in_range(number(), number(), number()) -> boolean().
is_in_range(X, Min, Max) ->
    (X >= Min) and (X =< Max).

-spec check_sensor(sensor(), [sensor()], {integer(), integer()}) -> 'false' | {'true', position()}.
check_sensor(Sensor, Sensors, {Min, Max}) ->
    Count = Sensor#sensor.beacon_distance + 1,
    StartingTopPos = add_to_pos(Sensor#sensor.at, {0, -Count}),
    Direction = [{Dir, Count} || Dir <- [{1, 1}, {-1, 1}, {-1, -1}, {1, -1}]],
    CheckFun = fun({PosX, PosY}) ->
        case (is_in_range(PosX, Min, Max)) and (is_in_range(PosY, Min, Max)) of
            false -> false;
            true ->
                case is_in_any_of_sensors_area({PosX, PosY}, Sensors) of
                    false ->
                        {true, {PosX, PosY}};
                    true ->
                        false
                end
        end
    end,
    iterate_sensor_outline(Sensor, StartingTopPos, Direction, CheckFun).

-spec is_in_any_of_sensors_area(position(), [sensor()]) -> boolean().
is_in_any_of_sensors_area(_, []) -> false;
is_in_any_of_sensors_area(Pos, [Sensor|Rest]) ->
    case is_point_in_sensor_area(Pos, Sensor) of
        true -> true;
        false -> is_in_any_of_sensors_area(Pos, Rest)
    end.

-spec is_point_in_sensor_area(position(), sensor()) -> boolean().
is_point_in_sensor_area(Point, Sensor) ->
    PointRange = distance(Sensor#sensor.at, Point),
    PointRange =< Sensor#sensor.beacon_distance.

-spec iterate_sensor_outline(sensor(), position(), [{position(), non_neg_integer()}], fun((position()) -> 'false' | {'true', position()})) -> 'false' | {'true', position()}.
iterate_sensor_outline(_, _, [], _) -> false;
iterate_sensor_outline(Sensor, CurrentPos, [{MoveDelta, MoveCount}|NextDirRest], Fun) ->
    case Fun(CurrentPos) of
        false ->
            NextDirection = case MoveCount =< 1 of
                true -> NextDirRest;
                false -> [{MoveDelta, MoveCount - 1}|NextDirRest]
            end,
            NextPos = add_to_pos(CurrentPos, MoveDelta),
            iterate_sensor_outline(Sensor, NextPos, NextDirection, Fun);
        Res -> Res
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_sensor(Filepath) of
        {ok, Sensors} ->
            Range = {0, 4000000},
            Multiplier = 4000000,
            find_distress_signal(Sensors, Range, Multiplier);
        error -> error
    end.