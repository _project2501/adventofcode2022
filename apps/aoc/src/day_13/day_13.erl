-module(day_13).
-export([solve/2]).
-mode(compile).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec process_line(io:device(), [integer()], [tuple()]) -> [tuple()].
process_line(Device, Pair, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            Tuple = list_to_tuple(lists:reverse(Pair)),
            lists:reverse([Tuple|Acc]);
        "\n" ->
            Tuple = list_to_tuple(lists:reverse(Pair)),
            process_line(Device, [], [Tuple|Acc]);
        Line ->
            ParsedLine = parse_line(string:trim(Line)),
            NewPair = [ParsedLine|Pair],
            process_line(Device, NewPair, Acc)
    end.

-spec parse_line(string()) -> any().
parse_line(String) ->
    {ok, Tokens, _} = erl_scan:string(String),
    {ok, Result} = erl_parse:parse_term(Tokens ++ [{dot,1} || element(1, lists:last(Tokens)) =/= dot]),
    Result.

-spec read_input(string()) -> 'error' | {'ok', [tuple()]}.
read_input(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, [], [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec compare_pairs([{list(), list()}, ...]) -> [{pos_integer(), 'false' | 'true' | 'undetermined'}].
compare_pairs(Input) ->
    compare_pairs(Input, 1, []).

-spec compare_pairs([{list(), list()}, ...], pos_integer(), [{pos_integer(), 'false' | 'true' | 'undetermined'}]) -> [{pos_integer(), 'false' | 'true' | 'undetermined'}].
compare_pairs([], _, Acc) -> lists:reverse(Acc);
compare_pairs([{Left, Right}|Rest], Index, Acc) ->
    Res = compare_values(Left, Right),
    NewAcc = [{Index, Res}|Acc],
    compare_pairs(Rest, Index + 1, NewAcc).

-spec compare_values(list() | integer(), list() | integer()) -> 'false' | 'true' | 'undetermined'.
compare_values(V1, V2) when is_integer(V1), is_integer(V2) -> compare_integers(V1, V2);
compare_values(V1, V2) when is_integer(V1), not is_integer(V2) -> compare_lists([V1], V2);
compare_values(V1, V2) when not is_integer(V1), is_integer(V2) -> compare_lists(V1, [V2]);
compare_values(V1, V2) when is_list(V1), is_list(V2) -> compare_lists(V1, V2).

-spec compare_lists(list(), list()) -> 'false' | 'true' | 'undetermined'.
compare_lists([], []) -> undetermined;
compare_lists([], Right) when Right =/= [] -> true;
compare_lists(Left, []) when Left =/= [] -> false;
compare_lists([L|RestL], [R|RestR]) ->
    case compare_values(L, R) of
        undetermined ->
            compare_lists(RestL, RestR);
        Val ->
            Val
    end.

-spec compare_integers(integer(), integer()) -> 'false' | 'true' | 'undetermined'.
compare_integers(V1, V2) when V1 > V2 -> false;
compare_integers(V1, V2) when V1 < V2 -> true;
compare_integers(_, _) -> undetermined.

-spec calculate_sum([{pos_integer(), 'false' | 'true' | 'undetermined'}]) -> non_neg_integer().
calculate_sum(PairResults) ->
    lists:foldl(
        fun({Index, Result}, Acc) ->
            case Result of
                true ->
                    Acc + Index;
                false ->
                    Acc
            end
        end, 0, PairResults).

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_input(Filepath) of
        {ok, Input} ->
            PairResults = compare_pairs(Input),
            calculate_sum(PairResults);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec flatten_input([{list(), list()}, ...]) -> [list()].
flatten_input(Input) ->
    flatten_input(Input, []).

-spec flatten_input([{list(), list()}], [list()]) -> [list()].
flatten_input([], Acc) -> Acc;
flatten_input([{L, R}|Rest], Acc) ->
    flatten_input(Rest, [L|[R|Acc]]).

-spec find_and_multiply_dividers([list(), ...], [list(), ...]) -> non_neg_integer().
find_and_multiply_dividers(Dividers, Input) ->
    lists:foldl(
        fun({Index, Val}, Acc) ->
            case lists:member(Val, Dividers) of
                true ->
                    Acc * Index;
                false ->
                    Acc
            end
        end,
        1, lists:zip(lists:seq(1, length(Input)), Input)).

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_input(Filepath) of
        {ok, Input} ->
            Dividers = [[[2]], [[6]]],
            NewInput = flatten_input([list_to_tuple(Dividers)|Input]),
            Sorted = lists:sort(fun compare_values/2, NewInput),
            find_and_multiply_dividers(Dividers, Sorted);
        error -> error
    end.