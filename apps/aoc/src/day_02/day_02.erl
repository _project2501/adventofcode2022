-module(day_02).
-export([solve/2]).
-define(SHAPE_ROCK, 1).
-define(SHAPE_PAPER, 2).
-define(SHAPE_SCISSORS, 3).
-define(RESULT_LOST, 0).
-define(RESULT_DRAW, 3).
-define(RESULT_WON, 6).

-type shape() :: ?SHAPE_PAPER | ?SHAPE_ROCK | ?SHAPE_SCISSORS.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

% Rock
-spec char_to_shape(char()) -> shape().
char_to_shape(Char) when Char == $A -> ?SHAPE_ROCK;
char_to_shape(Char) when Char == $X -> ?SHAPE_ROCK;

% Paper
char_to_shape(Char) when Char == $B -> ?SHAPE_PAPER;
char_to_shape(Char) when Char == $Y -> ?SHAPE_PAPER;

% Scissors
char_to_shape(Char) when Char == $C -> ?SHAPE_SCISSORS;
char_to_shape(Char) when Char == $Z -> ?SHAPE_SCISSORS.

-spec calc_round(shape(), shape()) -> non_neg_integer().
calc_round(Shape, Shape) ->
    Shape + ?RESULT_DRAW;
calc_round(Opponent, Me) ->
    case Opponent + Me of
        % paper wins rock
        3 -> eval_score(Me, ?SHAPE_PAPER);
        % rock wins scissors
        4 -> eval_score(Me, ?SHAPE_ROCK);
        % scissors wins paper
        5 -> eval_score(Me, ?SHAPE_SCISSORS)
    end.

-spec eval_score(shape(), shape()) -> non_neg_integer().
eval_score(MyShape, MyShape)            -> ?RESULT_WON + MyShape;
eval_score(MyShape, _WinningShape)      -> ?RESULT_LOST + MyShape.

-spec process_line_1(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_1(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            OpponentMove = char_to_shape(hd(Line)),
            MyMove = char_to_shape(lists:last(string:trim(Line))),
            process_line_1(Device, calc_round(OpponentMove, MyMove) + Acc)
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            process_line_1(Device, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

% Need to lose
-spec pick_shape(shape(), char()) -> shape().
pick_shape(OpponentMove, Instruction) when Instruction == $X, OpponentMove == ?SHAPE_ROCK       -> ?SHAPE_SCISSORS;
pick_shape(OpponentMove, Instruction) when Instruction == $X, OpponentMove == ?SHAPE_PAPER      -> ?SHAPE_ROCK;
pick_shape(OpponentMove, Instruction) when Instruction == $X, OpponentMove == ?SHAPE_SCISSORS   -> ?SHAPE_PAPER;

% Need to draw
pick_shape(OpponentMove, Instruction) when Instruction == $Y -> OpponentMove;

% Need to win
pick_shape(OpponentMove, Instruction) when Instruction == $Z, OpponentMove == ?SHAPE_ROCK       -> ?SHAPE_PAPER;
pick_shape(OpponentMove, Instruction) when Instruction == $Z, OpponentMove == ?SHAPE_PAPER      -> ?SHAPE_SCISSORS;
pick_shape(OpponentMove, Instruction) when Instruction == $Z, OpponentMove == ?SHAPE_SCISSORS   -> ?SHAPE_ROCK.

-spec process_line_2(io:device(), non_neg_integer()) -> non_neg_integer().
process_line_2(Device, Acc) ->
    case io:get_line(Device, "") of
        eof -> Acc;
        Line ->
            OpponentMove = char_to_shape(hd(Line)),
            Instruction = lists:last(string:trim(Line)),
            MyMove = pick_shape(OpponentMove, Instruction),
            process_line_2(Device, calc_round(OpponentMove, MyMove) + Acc)
    end.

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            file:open(Filepath, [read]),
            process_line_2(Device, 0);
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.