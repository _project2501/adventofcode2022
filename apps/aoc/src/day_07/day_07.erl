-module(day_07).
-export([solve/2]).
-define(SEPARATOR, "/").
-define(ROOT_DIR, ?SEPARATOR).

-record(file, {
    name :: string(),
    size :: non_neg_integer()}).
-type file() :: #file{}.

-record(dir, {
    name :: string(),
    content = [] :: dir() | file()}).
-type dir() :: #dir{}.

-record(filesys, {
    current_dir :: [string()],
    content = maps:new() :: map()}).
-type filesys() :: #filesys{}.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec parse_line(string()) -> {command, 'cd_up' | 'cd_root' | {'cd', string()} | ls } | dir() | file().
parse_line([Char|Rest]) when Char == $$ -> 
    Tokens = tl(string:lexemes([Char|Rest], " ")),
    Cmd = case lists:nth(1, Tokens) of
        "cd" ->
            case lists:nth(2, Tokens) of
                ".." -> cd_up;
                ?SEPARATOR -> cd_root;
                Dir -> {cd, Dir}
            end;
        "ls" -> ls
    end,
    { command, Cmd };
parse_line(Line) ->
    Tokens = string:lexemes(Line, " "),
    case hd(Tokens) of
        "dir" ->
            #dir{
                name = lists:nth(2, Tokens),
                content = []
            };
        FileSizeStr ->
            #file{
                name = lists:nth(2, Tokens),
                size = list_to_integer(FileSizeStr)
            }
    end.

-spec list_to_path([string()]) -> string().
list_to_path(List) ->
    join_path(lists:reverse(List)).

-spec add_line_to_filesystem(string(), filesys()) -> filesys().
add_line_to_filesystem(Line, Filesys) ->
    case parse_line(Line) of
        { command, Cmd } ->
            CurrentPath = Filesys#filesys.current_dir,
            case Cmd of
                cd_up ->
                    Filesys#filesys{current_dir = tl(CurrentPath)};
                cd_root ->
                    Filesys#filesys{current_dir = [?SEPARATOR]};
                {cd, Dir } ->
                    Filesys#filesys{current_dir = [Dir|CurrentPath]};
                ls -> Filesys
            end;
        Inode ->
            InodeName = case is_record(Inode, file) of
                true -> Inode#file.name;
                false -> Inode#dir.name
            end,
            CurrentPathList = Filesys#filesys.current_dir,
            CurrentPathStr = list_to_path(CurrentPathList),
            AbsPath = list_to_path([InodeName|CurrentPathList]),
            FilesysContent = Filesys#filesys.content,
            ParentDirEntry = maps:get(CurrentPathStr, FilesysContent),
            NewParentDirEntry = ParentDirEntry#dir{content = [InodeName|ParentDirEntry#dir.content]},
            NewFilesys = Filesys#filesys{
                content = maps:put(CurrentPathStr, NewParentDirEntry, Filesys#filesys.content)
            },
            NewFilesys#filesys{
                content = maps:put(AbsPath, Inode, NewFilesys#filesys.content)
            }
    end.

-spec get_entry(filesys(), string()) -> {'dir' | dir()} | {'file' | file()} | 'invalid_path'.
get_entry(Filesys, Path) ->
    Entry = maps:get(Path, Filesys#filesys.content, {}),
    case is_record(Entry, file) of
        true -> {file, Entry};
        false ->
            case is_record(Entry, dir) of
                true ->
                    {dir, Entry};
                false ->
                    invalid_path
            end
    end.

-spec join_path([string()]) -> string().
join_path([]) -> "";
join_path([?SEPARATOR]) -> ?SEPARATOR;
join_path(PathList) ->
    case hd(PathList) == ?SEPARATOR of
        true ->
            join_path(tl(PathList));
        false ->
            join_path_it(PathList, "")
    end.

-spec join_path_it([string()], string()) -> string().
join_path_it([], Res) -> Res;
join_path_it([El|Rest], Res) ->
    join_path_it(Rest, Res ++ ?SEPARATOR ++ string:trim(El, both, ?SEPARATOR)).

-spec get_dirs_and_files(filesys(), string()) -> {[file()], [dir()]}.
get_dirs_and_files(Filesys, DirPath) ->
    Initial = {[], []},
    case get_entry(Filesys, DirPath) of
        {dir, Dir} ->
            lists:foldl(
                fun(Entry, {Files, Dirs}) ->
                    EntryKey = join_path([DirPath, Entry]),
                    case get_entry(Filesys, EntryKey) of
                        {file, _} -> {[EntryKey|Files], Dirs};
                        {dir, _} -> {Files, [EntryKey|Dirs]};
                        _ -> {Files, Dirs}
                    end
                end, Initial, Dir#dir.content);
        _ -> Initial
    end.

-spec count_size(filesys(), string()) -> non_neg_integer().
count_size(Filesys, Path) ->
    count_size_it(Filesys, [Path], 0).

-spec count_size_it(filesys(), string(), non_neg_integer()) -> non_neg_integer().
count_size_it(_, [], Acc) -> Acc;
count_size_it(Filesys, [Path|Rest], Acc) ->
    case get_entry(Filesys, Path) of
        {file, File} ->
            count_size_it(Filesys, Rest, Acc + File#file.size);
        {dir, _} ->
            {Files, Dirs} = get_dirs_and_files(Filesys, Path),
            NewAcc = lists:foldl(
                fun(FilePath, Size) ->
                    case get_entry(Filesys, FilePath) of
                        {file, File} ->
                            Size + File#file.size;
                        _ -> Size
                    end
                end, Acc, Files
            ),
            count_size_it(Filesys, Rest ++ Dirs, NewAcc);
        _ -> Acc
    end.

-spec count_size_of_dirs_below_limit(filesys(), non_neg_integer()) -> non_neg_integer().
count_size_of_dirs_below_limit(Filesys, Limit) ->
    maps:fold(
        fun(K, V, Acc) ->
            case is_record(V, dir) of
                true ->
                    Size = count_size(Filesys, K),
                    case Size =< Limit of
                        true ->
                            Acc + Size;
                        false -> Acc
                    end;
                false -> Acc
            end
        end, 0, Filesys#filesys.content).

-spec process_line(string(), filesys()) -> filesys().
process_line(Device, Filesys) ->
    case io:get_line(Device, "") of
        eof -> Filesys;
        Line ->
            NewFilesys = add_line_to_filesystem(string:trim(Line), Filesys),
            process_line(Device, NewFilesys)
    end.

-spec new_fs() -> filesys().
new_fs() ->
    RootDir = #dir{
        name = ?ROOT_DIR,
        content = []
    },
    #filesys{
        current_dir = [?ROOT_DIR],
        content = maps:put(?ROOT_DIR, RootDir, maps:new())
    }.

-spec read_filesystem(string()) -> {'ok', filesys()} | 'error'.
read_filesystem(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, new_fs())};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_filesystem(Filepath) of
        {ok, Filesys} -> count_size_of_dirs_below_limit(Filesys, 100000);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec find_dir_to_delete(filesys(), non_neg_integer(), non_neg_integer()) -> non_neg_integer().
find_dir_to_delete(Filesys, DiskCapacity, NeededUnusedSpace) ->
    DiskUsage = count_size(Filesys, ?ROOT_DIR),
    UnusedSpace = DiskCapacity - DiskUsage,
    NeedsToFree = NeededUnusedSpace - UnusedSpace,
    Dirs = maps:fold(
        fun(K, V, Acc) ->
            case is_record(V, dir) of
                true -> [K|Acc];
                false -> Acc
            end
        end, [], Filesys#filesys.content),
    lists:foldl(
        fun(DirPath, Acc) ->
            DirSize = count_size(Filesys, DirPath),
            case (NeedsToFree - DirSize =< 0) and ((Acc == 0) or (DirSize =< Acc)) of
                true -> DirSize;
                false -> Acc
            end
        end, 0, Dirs).

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_filesystem(Filepath) of
        {ok, Filesys} -> find_dir_to_delete(Filesys, 70000000, 30000000);
        error -> error
    end.