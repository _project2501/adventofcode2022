-module(day_08).
-export([solve/2]).
-mode(compile).

-record(forest, {
    matrix = [] :: [[non_neg_integer()]],
    size_x :: non_neg_integer(),
    size_y :: non_neg_integer()}).
-type forest() :: #forest{}.

-type direction() :: 'east' | 'north' | 'south' | 'west'.
-type forest_iterate_accumulator() :: fun((pos_integer(), pos_integer(), forest(), non_neg_integer()) -> any()).

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec check_visible(pos_integer(), pos_integer(), forest()) -> boolean().
check_visible(1, _, _) -> true;
check_visible(_, 1, _) -> true;
check_visible(PosX, _, Forest) when Forest#forest.size_x == PosX -> true;
check_visible(_, PosY, Forest) when Forest#forest.size_y == PosY -> true;
check_visible(PosX, PosY, Forest) ->
    check_visible_in_direction(PosX, PosY, east, Forest) or
    check_visible_in_direction(PosX, PosY, west, Forest) or
    check_visible_in_direction(PosX, PosY, north, Forest) or
    check_visible_in_direction(PosX, PosY, south, Forest).

-spec get_start_end(pos_integer(), pos_integer(), direction(), forest()) -> {pos_integer(), pos_integer(), -1 | 1} | 'invalid_pos'.
get_start_end(1, _, west, _) -> invalid_pos;
get_start_end(PosX, _, east, Forest) when Forest#forest.size_x == PosX -> invalid_pos;
get_start_end(_, 1, north, _) -> invalid_pos;
get_start_end(_, PosY, south, Forest) when Forest#forest.size_y == PosY -> invalid_pos;
get_start_end(PosX, PosY, Dir, Forest) ->
    case Dir of
        west ->
            {PosX - 1, 1, -1};
        east ->
            {PosX + 1, Forest#forest.size_x, 1};
        north ->
            {PosY - 1, 1, -1};
        south ->
            {PosY + 1, Forest#forest.size_y, 1}
    end.

-spec gather_tree_positions(pos_integer(), pos_integer(), direction(), forest()) -> [pos_integer()].
gather_tree_positions(PosX, PosY, Dir, Forest) ->
    case get_start_end(PosX, PosY, Dir, Forest) of
        {Start, End, Step} ->
            lists:seq(Start, End, Step);
        invalid_pos -> []
    end.

-spec get_trees_in_line(pos_integer(), pos_integer(), direction(), forest()) -> [non_neg_integer()].
get_trees_in_line(PosX, PosY, Dir, Forest) ->
    TreePositions = gather_tree_positions(PosX, PosY, Dir, Forest),
    IsXAxis = lists:member(Dir, [east, west]),
    lists:map(
        fun(El) ->
            case IsXAxis of
                true ->
                    get_tree_height(El, PosY, Forest);
                false ->
                    get_tree_height(PosX, El, Forest)
            end
        end, TreePositions).

-spec get_tree_height(pos_integer(), pos_integer(), forest()) -> non_neg_integer().
get_tree_height(PosX, PosY, Forest) ->
    array:get(PosX - 1, array:get(PosY - 1, Forest#forest.matrix)).

-spec check_visible_in_direction(pos_integer(), pos_integer(), direction(), forest()) -> boolean().
check_visible_in_direction(PosX, PosY, Dir, Forest) ->
    Trees = get_trees_in_line(PosX, PosY, Dir, Forest),
    Tree = get_tree_height(PosX, PosY, Forest),
    is_tree_visible_against(Tree, Trees).

-spec is_tree_visible_against(non_neg_integer(), [non_neg_integer()]) -> boolean().
is_tree_visible_against(_, []) -> true;
is_tree_visible_against(Tree, [Current|_]) when Current >= Tree -> false;
is_tree_visible_against(Tree, [_|Rest]) -> is_tree_visible_against(Tree, Rest).

-spec iterate_forest_it(pos_integer(), pos_integer(), forest_iterate_accumulator(), forest(), any()) -> any().
iterate_forest_it(PosX, PosY, Fun, Forest, Acc) when PosX > Forest#forest.size_x ->
    case PosY == Forest#forest.size_y of
        true -> Acc;
        false -> iterate_forest_it(1, PosY + 1, Fun, Forest, Acc)
    end;
iterate_forest_it(PosX, PosY, Fun, Forest, Acc) ->
    NewAcc = Fun(PosX, PosY, Forest, Acc),
    iterate_forest_it(PosX + 1, PosY, Fun, Forest, NewAcc).

-spec iterate_forest(forest_iterate_accumulator(), forest(), any()) -> any().
iterate_forest(Fun, Forest, Acc) ->
    iterate_forest_it(1, 1, Fun, Forest, Acc).

-spec check_tree_visible(pos_integer(), pos_integer(), forest(), any()) -> any().
check_tree_visible(PosX, PosY, Forest, Acc) ->
    case check_visible(PosX, PosY, Forest) of
        true -> Acc + 1;
        false -> Acc
    end.

-spec count_visible_trees(forest()) -> non_neg_integer().
count_visible_trees(Forest) ->
    iterate_forest(fun check_tree_visible/4, Forest, 0).

-spec list_matrix_to_array_matrix([[non_neg_integer()]]) -> array:array(array:array(non_neg_integer())).
list_matrix_to_array_matrix(ListMatrix) ->
    SizeY = length(ListMatrix),
    lists:foldl(
        fun({Index, Row}, Acc) ->
            array:set(Index, array:fix(array:from_list(Row)), Acc)
        end,
        array:new([{size, SizeY}, {fixed, true}]),
        lists:zip(lists:seq(0, SizeY - 1), ListMatrix)).

-spec read_forest_line(io:device(), [[non_neg_integer()]]) -> forest().
read_forest_line(Device, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            #forest{
                matrix = list_matrix_to_array_matrix(lists:reverse(Acc)),
                size_x = length(hd(Acc)),
                size_y = length(Acc)
            };
        Line ->
            Numbers = lists:map(
                fun(El) ->
                    list_to_integer([El])
                end, string:trim(Line)),
            read_forest_line(Device, [Numbers|Acc])
    end.

-spec read_forest(string()) -> {'ok', [[non_neg_integer()]]} | 'error'.
read_forest(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, read_forest_line(Device, [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_forest(Filepath) of
        {ok, Forest} -> count_visible_trees(Forest);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec get_acceptable_trees(non_neg_integer(), [non_neg_integer()], forest(), [non_neg_integer()]) -> [non_neg_integer()].
get_acceptable_trees(_, [], _, Acc) -> Acc;
get_acceptable_trees(Tree, [Current|Rest], Forest, Acc) ->
    case Current < Tree of
        true -> get_acceptable_trees(Tree, Rest, Forest, [Current|Acc]);
        false -> [Current|Acc]
    end.

-spec calc_score_dir(non_neg_integer(), pos_integer(), pos_integer(), direction(), forest()) -> non_neg_integer().
calc_score_dir(Tree, PosX, PosY, Dir, Forest) ->
    TreesInLine = get_trees_in_line(PosX, PosY, Dir, Forest),
    AcceptableTrees = get_acceptable_trees(Tree, TreesInLine, Forest, []),
    length(AcceptableTrees).

-spec calc_tree_scenic_score(pos_integer(), pos_integer(), forest()) -> non_neg_integer().
calc_tree_scenic_score(PosX, PosY, Forest) ->
    Tree = get_tree_height(PosX, PosY, Forest),
    calc_score_dir(Tree, PosX, PosY, north, Forest) *
    calc_score_dir(Tree, PosX, PosY, south, Forest) *
    calc_score_dir(Tree, PosX, PosY, east, Forest) *
    calc_score_dir(Tree, PosX, PosY, west, Forest).

-spec compare_tree_scenic_score(pos_integer(), pos_integer(), forest(), non_neg_integer()) -> any().
compare_tree_scenic_score(1, _, _, Acc) -> Acc;
compare_tree_scenic_score(_, 1, _, Acc) -> Acc;
compare_tree_scenic_score(PosX, _, Forest, Acc) when Forest#forest.size_x == PosX -> Acc;
compare_tree_scenic_score(_, PosY, Forest, Acc) when Forest#forest.size_y == PosY -> Acc;
compare_tree_scenic_score(PosX, PosY, Forest, Acc) ->
    ScenicScore = calc_tree_scenic_score(PosX, PosY, Forest),
    case ScenicScore > Acc of
        true -> ScenicScore;
        false -> Acc
    end.

-spec find_the_best_tree_scenic_score(forest()) -> non_neg_integer().
find_the_best_tree_scenic_score(Forest) ->
    iterate_forest(fun compare_tree_scenic_score/4, Forest, 0).

-spec solve_2(string()) -> 'error' | non_neg_integer().
solve_2(Filepath) ->
    case read_forest(Filepath) of
        {ok, Forest} -> find_the_best_tree_scenic_score(Forest);
        error -> error
    end.