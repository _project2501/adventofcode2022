-module(day_10).
-export([solve/2]).
-define(CYCLES_TO_CHECK, [20, 60, 100, 140, 180, 220]).
-define(SCREEN_WIDTH, 40).
-define(SCREEN_HEIGHT, 6).
-define(SPRITE_WIDTH, 3).

-record(device_state, {
    x = 1 :: integer(),
    cycle = 0 :: non_neg_integer(),
    command_index = 0 :: non_neg_integer(),
    signal_strength_sum = 0 :: non_neg_integer(),
    future = maps:new() :: maps:map(),
    screen :: array:array(boolean())
}).
-type device_state() :: #device_state{}.
-type command() :: 'noop' | { addx, integer()}.

-spec solve(string(), string()) -> 'ok' | 'error'.
solve(File1, File2) ->
    io:format("Result 1: ~p~n", [solve_1(File1)]),
    io:format("Result 2: ~p~n", [solve_2(File2)]).

%%%%%%%%%%
% PART 1 %
%%%%%%%%%%

-spec parse_command(string()) -> command().
parse_command(Line) ->
    Tokens = string:tokens(Line, " "),
    case hd(Tokens) of
        "noop" ->
            noop;
        "addx" ->
            {addx, list_to_integer(hd(tl(Tokens)))}
end.

-spec start_processing(array:array(command()), non_neg_integer()) -> #device_state{}.
start_processing(Commands, CycleCount) ->
    State = new_device_state(),
    tick_cycle(Commands, CycleCount, State).

-spec start_new_cycle(device_state()) -> device_state().
start_new_cycle(State) ->
    Cycle = State#device_state.cycle,
    Future = State#device_state.future,
    SignalStrengths = case lists:member(Cycle, ?CYCLES_TO_CHECK) of
        false ->
            State#device_state.signal_strength_sum;
        true ->
            State#device_state.signal_strength_sum + State#device_state.x * Cycle
    end,
    State#device_state{
        future = maps:remove(Cycle, Future),
        signal_strength_sum = SignalStrengths
    }.

-spec queue_next_command(array:array(command()), device_state()) -> device_state().
queue_next_command(Commands, State) ->
    CommandIndex = State#device_state.command_index,
    Cycle = State#device_state.cycle,
    case CommandIndex =< array:size(Commands) of
        false -> State;
        true ->
            case maps:size(State#device_state.future) of
                0 ->
                    case array:get(CommandIndex, Commands) of
                        undefined -> State;
                        Command ->
                            Duration = get_command_cycle_duration(Command),
                            State#device_state{
                                future = maps:put(Cycle + Duration, Command, State#device_state.future),
                                command_index = State#device_state.command_index + 1
                            }
                        end;
                _ ->
                    State
            end
    end.

-spec is_pixel_lit(non_neg_integer(), integer()) -> boolean().
is_pixel_lit(Cycle, X) ->
    SideMin = (?SPRITE_WIDTH - 1) div 2,
    SideMax = ?SPRITE_WIDTH - 1 - SideMin,
    SpriteRangeMin = X - SideMin,
    SpriteRangeMax = X + SideMax,
    PixelIndex = Cycle rem ?SCREEN_WIDTH,
    (PixelIndex >= SpriteRangeMin) and (PixelIndex =< SpriteRangeMax).

-spec update_screen(device_state()) -> device_state().
update_screen(State) ->
    Cycle = State#device_state.cycle,
    X = State#device_state.x,
    case Cycle < array:size(State#device_state.screen) of
        false -> State;
        true ->
            Value = is_pixel_lit(Cycle, X),
            NewScreen = array:set(Cycle, Value, State#device_state.screen),
            State#device_state{
                screen = NewScreen
            }
    end.

-spec exec_future(maps:map(), device_state()) -> device_state().
exec_future(Future, State) ->
    Cycle = State#device_state.cycle,
    case maps:get(Cycle, Future, badkey) of
        {addx, N} ->
            X = State#device_state.x,
            State#device_state{
                x = X + N
            };
        _ ->
            State
    end.

-spec next_cycle(device_state()) -> device_state().
next_cycle(State) ->
    Cycle = State#device_state.cycle,
    State#device_state{
        cycle = Cycle + 1
    }.

-spec get_command_cycle_duration(command()) -> non_neg_integer().
get_command_cycle_duration(noop) -> 1;
get_command_cycle_duration({addx, _}) -> 2.

-spec tick_cycle(array:array(command()), non_neg_integer(), #device_state{}) -> #device_state{}.
tick_cycle(_, CycleCount, State) when State#device_state.cycle > CycleCount -> State;
tick_cycle(Commands, CycleCount, State0) ->
    FutureToExecute = State0#device_state.future,
    State1 = start_new_cycle(State0),
    State2 = queue_next_command(Commands, State1),
    State3 = exec_future(FutureToExecute, State2),
    State4 = update_screen(State3),
    State5 = next_cycle(State4),
    tick_cycle(Commands, CycleCount, State5).

-spec new_screen() -> array:array(boolean()).
new_screen() ->
    Size = ?SCREEN_WIDTH * ?SCREEN_HEIGHT,
    array:new([{size, Size}, {fixed, true}, {default, false}]).

-spec new_device_state() -> #device_state{}.
new_device_state() ->
    #device_state{
        x = 1,
        cycle = 0,
        command_index = 0,
        signal_strength_sum = 0,
        future = maps:new(),
        screen = new_screen()
    }.

-spec calculate_signal_sum(#device_state{}) -> non_neg_integer().
calculate_signal_sum(State) ->
    State#device_state.signal_strength_sum.

-spec process_line(io:device(), [command()]) -> array:array(command()).
process_line(Device, Acc) ->
    case io:get_line(Device, "") of
        eof ->
            array:from_list(lists:reverse(Acc));
        Line ->
            Command = parse_command(string:trim(Line)),
            process_line(Device, [Command|Acc])
    end.

-spec read_commands_from_file(string()) -> 'error' | {'ok', array:array(command())}.
read_commands_from_file(Filepath) ->
    case file:open(Filepath, [read]) of
        {ok, Device} ->
            {ok, process_line(Device, [])};
        {error, Reason} ->
            io:format(standard_error, "Unable to read '~p', error: ~p~n", [Filepath, Reason]),
            error
    end.

-spec solve_1(string()) -> 'error' | non_neg_integer().
solve_1(Filepath) ->
    case read_commands_from_file(Filepath) of
        {ok, Commands} ->
            CycleCount = lists:max(?CYCLES_TO_CHECK),
            State = start_processing(Commands, CycleCount),
            calculate_signal_sum(State);
        error -> error
    end.

%%%%%%%%%%
% PART 2 %
%%%%%%%%%%

-spec screen_to_string(#device_state{}) -> string().
screen_to_string(State) ->
    dump_screen_line(0, State, []).

-spec dump_screen_line(non_neg_integer(), #device_state{}, [string()]) -> string().
dump_screen_line(ScreenLineIndex, _, Acc) when ScreenLineIndex >= ?SCREEN_HEIGHT ->
    lists:reverse(Acc);
dump_screen_line(ScreenLineIndex, State, Acc) ->
    StartIndex = ScreenLineIndex * ?SCREEN_WIDTH,
    EndIndex = StartIndex + ?SCREEN_WIDTH - 1,
    CurrentLine = lists:map(
        fun(Index) ->
            case array:get(Index, State#device_state.screen) of
                true -> $#;
                false -> $.
            end
        end, lists:seq(StartIndex, EndIndex)),
    NewAcc = [CurrentLine|Acc],
    dump_screen_line(ScreenLineIndex + 1, State, NewAcc).

-spec solve_2(string()) -> 'error' | string().
solve_2(Filepath) ->
    case read_commands_from_file(Filepath) of
        {ok, Commands} ->
            CycleCount = ?SCREEN_WIDTH * ?SCREEN_HEIGHT,
            State = start_processing(Commands, CycleCount),
            screen_to_string(State);
        error -> error
    end.